# Conway's Game of Life

![IMAGE_DESCRIPTION](images/header.png)

Welcome to the Conway's Game of Life repository! This project is a simple implementation of the famous cellular automaton devised by mathematician John Conway in 1970.

## About the Game
Conway's Game of Life is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. Players create an initial configuration and observe how it evolves. It is Turing complete and can simulate a universal constructor or any other Turing machine.

Learn more about Conway's Game of Life on [Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

## Features
- Pure vanilla JavaScript implementation
- Interactive grid where you can set the initial state
- Start, pause, and reset controls
- Сustomize cell size, simulation speed and population density
- Simple, clean UI

## Getting Started

### Prerequisites

To run this project, you'll need a modern web browser.

### Installation

1. Clone the repository:
   ```bash
   git@gitlab.com:ae_anisimov/game-of-life.git
   ```
2. Navigate to the project directory:
    ```bash
    cd game-of-life/public
    ```
3. Open index.html in your web browser to start the game.

### Usage
- Use the control buttons to start, pause, or reset the simulation.
- Сhange cell size, simulation speed and population density in the settings window.

### Project Structure
```
game-of-life
├── images                 # Folder with images
├── public                 # Folder with game
│        ├── app.js        # Main JavaScript file for the game and UI logic
│        ├── favicon.ico   # Main Favicon 
│        ├── index.html    # Main HTML file
│        └── style.css     # Main styles for the game
├── .gitignore             # Project untracked files for Git
├── .gitlab-ci.yml         # Project CI/CD config
├── LICENSE                # Project LICENSE file
└── README.md              # Project README file
```

### Contributing
Contributions are welcome! Please fork this repository, make your changes, and submit a pull request.

1. Fork the project
2. Create your feature branch (`git checkout -b feature/your-feature`)
3. Commit your changes (`git commit -m 'Add some feature'`)
4. Push to the branch (`git push origin feature/your-feature`)
5. Open a pull request

### License
This project is licensed under the MIT License. See the [LICENSE](./LICENSE) file for details.

### Acknowledgements
- John Conway for creating the Game of Life
- [Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) for the detailed explanation

---
#### Enjoy the game and happy coding!