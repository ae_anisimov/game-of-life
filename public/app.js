const WIN_WIDTH = window.innerWidth;
const WIN_HEIGHT = window.innerHeight;

const PLAY_BTN = document.getElementById('play_btn');
const SETTINGS = document.getElementById('settings_modal');
const CELL_SIZE_INPUT = document.getElementById('cell_size_input');
const STEP_TIME_INPUT = document.getElementById('step_time_input');
const DENSITY_INPUT = document.getElementById('density_input');
const CANVAS = document.getElementById('game_canvas');

CANVAS.width = WIN_WIDTH;
CANVAS.height = WIN_HEIGHT;

const StorageConst = {
    IS_RUNNING: 'is_running',
    CELL_SIZE: 'cell_size',
    STEP_TIME: 'step_time',
    DENSITY: 'density',
}

const GameConst = {
    SIZE: 3,
    STEP_TIME: 10,
    DENSITY: 0.075,
}


class GameController {
    constructor() {
        this.is_running = _loadFormStorage(StorageConst.IS_RUNNING, 1);

        this.size = _loadFormStorage(StorageConst.CELL_SIZE, GameConst.SIZE);
        this.step_time = _loadFormStorage(StorageConst.STEP_TIME, GameConst.STEP_TIME);
        this.density = _loadFormStorage(StorageConst.DENSITY, GameConst.DENSITY);

        this.cols = Math.floor(window.innerWidth / this.size);
        this.rows = Math.floor(window.innerHeight / this.size);
        this.ctx = CANVAS.getContext('2d');

        this.color = getComputedStyle(CANVAS).getPropertyValue("--custom-green");

        this.state = [];
        this.grid = [];

        this.run();
    }

    run() {
        // randomize
        for (let i = 0; i < this.cols; i++) {
            this.grid[i] = [];
            this.state[i] = [];
            for (let j = 0; j < this.rows; j++) {
                this.grid[i][j] = Math.random() < this.density;
                this.state[i][j] = this.grid[i][j];
            }
        }
        // first iter
        this.iterate();
    }

    iterate() {
        this._updateGrid();
        this._drawGrid();
        this.state = this.grid;

        if (this.is_running) {
            setTimeout(() => {
                this.iterate();
            }, this.step_time);
        }
    }

    _updateGrid() {
        for (let i = 0; i < this.cols; i++) {
            for (let j = 0; j < this.rows; j++) {
                this.grid[i][j] = this._getNewState(i, j);
            }
        }
    }

    _drawGrid() {
        this.ctx.clearRect(0, 0, WIN_WIDTH, WIN_HEIGHT);
        for (let i = 0; i < this.cols; i++) {
            for (let j = 0; j < this.rows; j++) {
                if (this.grid[i][j]) {
                    this._drawCell(i, j);
                }
            }
        }
    }

    _getNewState(i, j) {
        let liveNeighbors = 0;
        let iMinus = i - 1 >= 0;
        let iPlus = i + 1 < this.cols;
        let jMinus = j - 1 >= 0;
        let jPlus = j + 1 < this.rows;

        if (iMinus && jMinus && this.state[i - 1][j - 1])
            liveNeighbors++;

        if (iMinus && this.state[i - 1][j])
            liveNeighbors++;

        if (iMinus && jPlus && this.state[i - 1][j + 1])
            liveNeighbors++;

        if (iPlus && jMinus && this.state[i + 1][j - 1])
            liveNeighbors++;

        if (iPlus && this.state[i + 1][j])
            liveNeighbors++;

        if (iPlus && jPlus && this.state[i + 1][j + 1])
            liveNeighbors++;

        if (jMinus && this.state[i][j - 1])
            liveNeighbors++;

        if (jPlus && this.state[i][j + 1])
            liveNeighbors++;

        return (this.state[i][j] && liveNeighbors === 2) ||
            (this.state[i][j] && liveNeighbors === 3) ||
            (!this.state[i][j] && liveNeighbors === 3);
    }

    _drawCell(x, y) {
        this.ctx.save();
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(x * this.size, y * this.size, this.size, this.size);
        this.ctx.restore();
    }
}

class UIController {
    constructor(game) {
        this.game = game;

        this._initUI();
    }

    _initUI() {
        PLAY_BTN.classList.add(game.is_running ? 'fa-pause' : 'fa-play');
        document.body.onkeyup = (e) => {
            if (e.key === " " || e.code === "Space") {
                this.startOrStopGame();
            }
        }
    }

    startOrStopGame() {
        const is_running = this.game.is_running ? 0 : 1;
        this.game.is_running = is_running;
        localStorage.setItem(StorageConst.IS_RUNNING, String(is_running));

        PLAY_BTN.classList.remove(is_running ? 'fa-play' : 'fa-pause');
        PLAY_BTN.classList.add(is_running ? 'fa-pause' : 'fa-play');

        if (is_running)
            this.game.iterate();
    }

    openSettings() {
        SETTINGS.style.display = 'block';
        CELL_SIZE_INPUT.value = this.game.size;
        STEP_TIME_INPUT.value = this.game.step_time;
        DENSITY_INPUT.value = this.game.density;
    }

    closeSettings() {
        SETTINGS.style.display = 'none';
    }

    saveSettings() {
        localStorage.setItem(StorageConst.CELL_SIZE, CELL_SIZE_INPUT.value);
        localStorage.setItem(StorageConst.STEP_TIME, STEP_TIME_INPUT.value);
        localStorage.setItem(StorageConst.DENSITY, DENSITY_INPUT.value);
        reload();
    }

    restoreSettings() {
        CELL_SIZE_INPUT.value = GameConst.SIZE;
        STEP_TIME_INPUT.value = GameConst.STEP_TIME;
        DENSITY_INPUT.value = GameConst.DENSITY;
    }
}

function _loadFormStorage(key, defaultValue) {
    const value = localStorage.getItem(key)
    if (value !== null) {
        if (_isDigit(value)) {
            return parseInt(value);
        }
        return value;
    }
    return defaultValue;
}

function _isDigit(str) {
    return /^\d+$/.test(str);
}

function reload() {
    location.reload();
}
